# CMake generated Testfile for 
# Source directory: C:/opencv/opencv_source/3rdparty
# Build directory: C:/opencv/opencv_bin_win32/3rdparty
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(flann)
SUBDIRS(lapack)
SUBDIRS(zlib)
SUBDIRS(libjasper)
SUBDIRS(libjpeg)
SUBDIRS(libpng)
SUBDIRS(libtiff)
