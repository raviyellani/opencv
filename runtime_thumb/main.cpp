//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft shared
// source or premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license agreement,
// you are not authorized to use this source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the SOURCE.RTF on your install media or the root of your tools installation.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <commdlg.h>
#include "opencv_demo_thumb.h"

#include "resource.h"
#include "CameraDriverTest.h"

void ParseFile ( TCHAR *filename ) ;
void initVM010 ( void ) ;

#define countof(x) (sizeof(x) / sizeof(x[0]))

static CCameraDriverTest g_CameraDriver;
static BOOL g_bPreviewAvailable = FALSE;
static BOOL g_bCaptureAvailable = FALSE;
static BOOL g_bStillAvailable = FALSE;
static HINSTANCE ghInstance = NULL;
BOOL Singlechecked;
BOOL saveraw = FALSE;
DWORD nover = 0;
BOOL resize_window = FALSE;
HWND rf, cb;	// command bar handle;
BOOL haswarn = FALSE;
DWORD wnd_sizex = 376;
DWORD wnd_sizey = 240;
OPENFILENAME ofn;
TCHAR pszPath[MAX_PATH+1] = L"";
TCHAR szFilter[MAX_PATH+1] = L"txt";

extern DWORD halbe;

DWORD camType = 0;	// 0 = color, 1 = mono
DWORD useReg = 1; 	// 1 = ja, 0 = nein
int g_nCameraDriverIndex = 0;
int g_nPreviewFormatIndex = 0;
int g_nCaptureFormatIndex = 0;
int g_nStillFormatIndex = 0;

int InformationArray[50];
HWND wregArray[25];
int runStream = 1;

// dll implementation function
#define MAXMODULE 50
typedef void (WINAPI*cfunc)();

//cfunc RunOpenCV;



typedef struct _BUTTONINFO
{
    int nButtonStyle;
    TCHAR *tszButtonName;
    int nButtonState;
    HWND hwndButton;
    int nButtonWidth;
    int nButtonHeight;
} BUTTONINFO;

#define BUTTON_WIDTH 70
#define BUTTON_HEIGHT 15
#define BUTTON_SPACING 2
#define WINDOW_SPACING 2

// this enum corresponds to the button entries.
enum
{
    COMMAND_CAPTURERUN,
    COMMAND_CAPTUREPAUSE,
    COMMAND_STILL_BMP,
	COMMAND_STILL_RAW,
	COMMAND_COL_NAT,
	COMMAND_COL_NEO,
	COMMAND_COL_LAM,
	COMMAND_COL_111
};

BUTTONINFO gButtonInfo [] = 
{
    {BS_PUSHBUTTON, TEXT("Live"), 0, NULL, BUTTON_WIDTH, BUTTON_HEIGHT },
    {BS_PUSHBUTTON, TEXT("Pause"), 0, NULL, BUTTON_WIDTH, BUTTON_HEIGHT },
    {BS_PUSHBUTTON, TEXT("Save BMP"), 0, NULL, BUTTON_WIDTH, BUTTON_HEIGHT },
	{BS_PUSHBUTTON, TEXT("detect"), 0, NULL, BUTTON_WIDTH, BUTTON_HEIGHT },
};
//	{BS_PUSHBUTTON, TEXT("Nature"), 0, NULL, BUTTON_WIDTH , BUTTON_HEIGHT },
//	{BS_PUSHBUTTON, TEXT("Neon"), 0, NULL, BUTTON_WIDTH , BUTTON_HEIGHT },
//	{BS_PUSHBUTTON, TEXT("Bulb"), 0, NULL, BUTTON_WIDTH , BUTTON_HEIGHT },
//	{BS_PUSHBUTTON, TEXT("Off"), 0, NULL, BUTTON_WIDTH, BUTTON_HEIGHT },
//};

void WriteI2C ( unsigned char reg, unsigned short val, char mode ) {
	unsigned short wval = 0;
	unsigned short oval = 0;
	BOOL b;
	
	b = g_CameraDriver.ReadRegister(reg, (DWORD*)&oval);

	switch ( mode ) {
		case 's':
		case 'S':	wval = val;
					break;
		case 'a':
		case 'A':	wval = oval & val;
					break;
		case 'o':
		case 'O':	wval = oval | val;
					break;
		case 'x':
		case 'X':	wval = oval ^ val;
					break;
	}
	RETAILMSG(1,(TEXT("CAM I2C change REG 0x%x from 0x%x to 0x%x\r\n"), reg, oval, wval));
	g_CameraDriver.WriteRegister(reg, wval);
}

BOOL ReadRegistryData( DWORD *cameraType, DWORD *useReg ) 
{
    LONG  regError;
    HKEY  hKey;
    DWORD dwDataSize;
	
	
    // Open the registry key
    regError = RegOpenKeyEx(HKEY_LOCAL_MACHINE,TEXT("Drivers\\BuiltIn\\Camera"),0,KEY_ALL_ACCESS,&hKey);
    if (regError != ERROR_SUCCESS)
    {
		*cameraType = 0;
        return (FALSE);
    }

    dwDataSize = sizeof(DWORD);
    regError   = RegQueryValueEx(hKey,L"CameraType", NULL, NULL,(LPBYTE)cameraType,&dwDataSize);
    if (regError != ERROR_SUCCESS)
    {
		*cameraType = 0;
	}

    dwDataSize = sizeof(DWORD);
    regError   = RegQueryValueEx(hKey,L"NoRegFile", NULL, NULL,(LPBYTE)useReg,&dwDataSize);
    if (regError != ERROR_SUCCESS)
    {
		*useReg = 1;
	}
	*cameraType = 0;

	RETAILMSG(1,(TEXT("Camera Registry data read ... type  =%d \r\n"), *cameraType));
	RETAILMSG(1,(TEXT("                          ... noReg =%d \r\n"), *useReg));
	RegCloseKey(hKey);
	return (TRUE);
}

//changed void SaveRegistryData( USHORT *path, DWORD save ) 
void SaveRegistryData( const wchar_t *path, DWORD save ) 
{
    LONG  regError;
    HKEY  hKey;
    DWORD dwDataSize;
	
    // Open the registry key
    regError = RegOpenKeyEx(HKEY_LOCAL_MACHINE,TEXT("Drivers\\BuiltIn\\Camera"),0,0,&hKey);
    if (regError != ERROR_SUCCESS)
    {
        RETAILMSG(1,(TEXT("Failed opening HKEY_LOCAL_MACHINE\\Drivers\\BuiltIn\\Camera\r\n")));
        return;
    }

	dwDataSize = wcslen(path) * 2;
	RegSetValueEx(hKey,L"RegFile", NULL, REG_SZ,(LPBYTE)path, dwDataSize );
	
	dwDataSize = sizeof(DWORD);
	RegSetValueEx(hKey,L"NoRegFile", NULL, REG_SZ,(LPBYTE)&save, dwDataSize );
	
	RETAILMSG(1,(TEXT("Camera Registry data write ... path  =%s \r\n"), path));
	RETAILMSG(1,(TEXT("                           ... noReg =%d \r\n"), useReg));
	
	RegCloseKey(hKey);
}

/* this function is called as soon as an input of any kind of user interface (mouse, keyboard, touch) occurs */
HWND wreg, wval, wreg1;
HWND wreg10, wregpopup;

LRESULT APIENTRY WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    RECT rc;
	unsigned char status[20];
    int index;
	CS_DATARANGE_VIDEO csFormat;

    GetClientRect(hwnd, &rc);

    switch(msg)
    {
        case WM_CREATE:

			wreg10 = CreateWindow(L"Edit",  L"no processing done", WS_BORDER | WS_VISIBLE | ES_LEFT |ES_READONLY, 
                                   10, 350, 150, 30,  // set size in WM_SIZE message 
                                    hwnd, (HMENU) 0x1234, ghInstance, NULL); 

            // initialize our buttons.

            GetClientRect(hwnd, &rc);

						// ist die fenstergroesse ...
			if ( rc.bottom < 280 ) {
				wnd_sizex = rc.right = 192;
				wnd_sizey = rc.bottom = 144;
				resize_window = TRUE;
			}			

			rc.top = wnd_sizey + 8;
			rc.left = BUTTON_SPACING + BUTTON_SPACING + 10;
			
			
            for(index = 0; index < countof(gButtonInfo); index++)
            {
                gButtonInfo[index].hwndButton = CreateWindow(TEXT("Button"), 
                                                                    gButtonInfo[index].tszButtonName,
                                                                    WS_CHILD | WS_VISIBLE | gButtonInfo[index].nButtonStyle,
                                                                    rc.left, 
                                                                    rc.top,
                                                                    gButtonInfo[index].nButtonWidth,
                                                                    gButtonInfo[index].nButtonHeight,
                                                                    hwnd,
                                                                    (HMENU) index, // the index is the identifier for the command
                                                                    ghInstance,
                                                                    NULL);
                rc.left += (gButtonInfo[index].nButtonWidth + BUTTON_SPACING);

				// monochrome cam
				//if (( camType==1 ) && ( index >= 3) ) break;
					
				// we will not produce more elemets so just stop the progess
				

				// while balance...
				if (index == 3) { 
					rc.left = BUTTON_SPACING + BUTTON_SPACING + 10; 
					rc.top += BUTTON_HEIGHT + BUTTON_SPACING; 
					
					//wreg1 = CreateWindow(L"Edit",  L"White Balance", WS_CHILD | WS_VISIBLE | ES_LEFT |ES_READONLY, 
     //                               rc.left, rc.top, 90, 15,  // set size in WM_SIZE message 
     //                               hwnd, (HMENU) 0x1111, ghInstance, NULL); 
		
					rc.top += BUTTON_HEIGHT + BUTTON_SPACING; 
				}

            }
			// register - windows ..
			rc.left = BUTTON_SPACING + BUTTON_SPACING + 10; 
			rc.top += BUTTON_HEIGHT + BUTTON_SPACING; 							
			
   //         wreg1 = CreateWindow(L"Edit",   L"Reg. 0x",
   //                                 WS_CHILD | WS_VISIBLE | ES_LEFT |ES_READONLY, 
   //                                 rc.left ,rc.top,45,15,
   //                                 hwnd, (HMENU) 0x1111, ghInstance, NULL); 

   //         wreg = CreateWindow(L"Edit", L"0f",
			//						WS_CHILD | WS_VISIBLE |  ES_LEFT | ES_MULTILINE, 
   //                                 rc.left + 50, rc.top, 18, 15,  
   //                                 hwnd,        // parent window 
   //                                 (HMENU) 0x1111,  ghInstance, NULL);
			//SendMessage(wreg, CB_LIMITTEXT , 2, 0); 
			//CreateWindow(TEXT("Button"), TEXT("RD"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
   //                                      rc.left + 65, rc.top, 25, BUTTON_HEIGHT,
   //                                      hwnd, (HMENU) 0x1113, ghInstance, NULL);
			//wreg1 = CreateWindow(L"Edit",   L"0x",
   //                                 WS_CHILD | WS_VISIBLE | ES_LEFT |ES_READONLY, 
   //                                 rc.left +95,rc.top,15,15,
   //                                 hwnd, (HMENU) 0x1111, ghInstance, NULL); 
			//						
   //         wval = CreateWindow(L"Edit",  L"00",
   //                                 WS_CHILD | WS_VISIBLE | ES_LEFT | ES_MULTILINE, 
   //                                 rc.left + 112, rc.top, 31, 15,  
   //                                 hwnd, (HMENU) 0x1112, ghInstance, NULL); 
			//SendMessage(wval, CB_LIMITTEXT , 4, 0); 
			//CreateWindow(TEXT("Button"), TEXT("WR"),
   //                                      WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
   //                                      rc.left+142, rc.top,25,BUTTON_HEIGHT,
   //                                      hwnd,(HMENU) 0x1114, ghInstance,NULL);

			//rf = CreateWindow(TEXT("Button"), TEXT("RegFile"),
   //                                      WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
   //                                      rc.left+192, rc.top,55,BUTTON_HEIGHT,
   //                                      hwnd,(HMENU) 0x1115, ghInstance,NULL);
#if 0										 
			CreateWindow(TEXT("Button"), TEXT("NoReg"),
                                         WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
                                         rc.left+236, rc.top,50,BUTTON_HEIGHT,
                                         hwnd,(HMENU) 0x1116, ghInstance,NULL);										 
#else
		/*	CreateWindow(TEXT("Button"), TEXT(""),
										WS_VISIBLE | WS_CHILD | BS_CHECKBOX,
										rc.left+175, rc.top,BUTTON_HEIGHT,BUTTON_HEIGHT,
                                        hwnd,(HMENU) 0x1116, ghInstance,NULL);		*/								 
			
		/*	if ( useReg ) {
				CheckDlgButton(hwnd, 0x1116, BST_CHECKED);
				EnableWindow(rf, TRUE);
				useReg = 1;
			} else {
				CheckDlgButton(hwnd, 0x1116, BST_UNCHECKED);
				EnableWindow(rf, FALSE);
				useReg = 0;
			}*/

			/*CreateWindow(TEXT("Button"), TEXT("Trig."),
										WS_VISIBLE | WS_CHILD | BS_CHECKBOX,
										rc.left+255, rc.top,55,BUTTON_HEIGHT,
                                        hwnd,(HMENU) 0x1117, ghInstance,NULL);	*/									 			
			Singlechecked = FALSE;
#endif										 
			// die darstellung ist fertig ...
			// ----------------------------

            if(FALSE == g_CameraDriver.DetermineCameraAvailability())
            {
                RETAILMSG(1, (TEXT( "CameraApp : Failed to find the camera driver")));
                PostQuitMessage(0);
                return 0;
            }

            if(FALSE == g_CameraDriver.SelectCameraDevice(g_nCameraDriverIndex))
            {
                RETAILMSG(1, (TEXT( "CameraApp : Failed to select the camera driver.")));
                PostQuitMessage(0);
                return 0;
            }

            if(FALSE == g_CameraDriver.InitializeDriver())
            {
                RETAILMSG(1, (TEXT( "CameraApp : Failed to select the camera driver.")));
                PostQuitMessage(0);
                return 0;
            }


            if(g_bPreviewAvailable)
            {

                RETAILMSG(1, (TEXT( "CameraApp : Creating preview stream")));

                if(FALSE == g_CameraDriver.GetFormatInfo(STREAM_PREVIEW, g_nPreviewFormatIndex, &csFormat))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : GetFormatInfo on the preview failed")));
                    PostQuitMessage(0);
                    return 0;
                }

                // retrieve the client area, compensate for some spacing between the windows,
                // calculate the upper left quadrent of the window, compare the quadrent to the
                // supported video format, if the supported format is smaller than use it, otherwise
                // shrink the image to fit the quadrent.
				
                GetClientRect(hwnd, &rc);

                rc.top += WINDOW_SPACING;
                rc.left = WINDOW_SPACING;
                rc.bottom = rc.top + wnd_sizey;
				rc.right = rc.left + wnd_sizex;


                if(FALSE == g_CameraDriver.CreateStream(STREAM_PREVIEW, hwnd, rc))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : CreateStream on the preview failed")));
                    PostQuitMessage(0);
                    return 0;
                }

                if(FALSE == g_CameraDriver.SelectVideoFormat(STREAM_PREVIEW, g_nPreviewFormatIndex))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : SelectVideoFormat on the preview failed")));
                    PostQuitMessage(0);
                    return 0;
                }

                if(FALSE == g_CameraDriver.SetupStream(STREAM_PREVIEW))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : SetupSTream on the preview failed")));
                    PostQuitMessage(0);
                    return 0;
                }
	
            }

            if(g_bCaptureAvailable)
            {
                RETAILMSG(1, (TEXT( "CameraApp : Creating capture stream")));

                if(FALSE == g_CameraDriver.GetFormatInfo(STREAM_CAPTURE, g_nCaptureFormatIndex, &csFormat))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : GetFormatInfo on the capture failed")));
                    PostQuitMessage(0);
                    return 0;
                }

                // retrieve the client area, compensate for some spacing between the windows,
                // calculate the upper right quadrent of the window, compare the quadrent to the
                // supported video format, if the supported format is smaller than use it, otherwise
                // shrink the image to fit the quadrent.

                GetClientRect(hwnd, &rc);
                rc.top  = WINDOW_SPACING;
                rc.left = WINDOW_SPACING;
                rc.bottom = rc.top + 0;
                rc.right = rc.left + 0;

                if(FALSE == g_CameraDriver.CreateStream(STREAM_CAPTURE, hwnd, rc))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : CreateStream on the capture failed")));
                    PostQuitMessage(0);
                    return 0;
                }

                if(FALSE == g_CameraDriver.SelectVideoFormat(STREAM_CAPTURE, g_nCaptureFormatIndex))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : SelectVideoFormat on the capture failed")));
                    PostQuitMessage(0);
                    return 0;
                }   

                if(FALSE == g_CameraDriver.SetupStream(STREAM_CAPTURE))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : SetupSTream on the capture failed")));
                    PostQuitMessage(0);
                    return 0;
                }
			}

            if(g_bStillAvailable)
            {
                RETAILMSG(1, (TEXT( "CameraApp : Creating still window")));

                if(FALSE == g_CameraDriver.GetFormatInfo(STREAM_STILL, g_nStillFormatIndex, &csFormat))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : GetFormatInfo on the still failed")));
                    PostQuitMessage(0);
                    return 0;
                }

                // retrieve the client area, compensate for some spacing between the windows,
                // calculate the lower right quadrent of the window, compare the quadrent to the
                // supported video format, if the supported format is smaller than use it, otherwise
                // shrink the image to fit the quadrent.
                GetClientRect(hwnd, &rc);

				// Hide window ..
                rc.top = 195;
                rc.left =195; // WINDOW_SPACING;
                rc.bottom = rc.top; 
                rc.right = rc.left; 

                if(FALSE == g_CameraDriver.CreateStream(STREAM_STILL, hwnd, rc))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : CreateStream on the still failed")));
                    PostQuitMessage(0);
                    return 0;
                }
				/* still bleibt still .... */
                if(FALSE == g_CameraDriver.SelectVideoFormat(STREAM_STILL, g_nStillFormatIndex))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : SelectVideoFormat on the still failed")));
                    PostQuitMessage(0);
                    return 0;
                }

                if(FALSE == g_CameraDriver.SetupStream(STREAM_STILL))
                {
                    RETAILMSG(1, (TEXT( "CameraApp : SetupSTream on the still failed")));
                    PostQuitMessage(0);
                    return 0;
                }
            }				
			
			if ( g_bPreviewAvailable ) 	g_CameraDriver.SetState( STREAM_PREVIEW, CSSTATE_PAUSE);
			if ( g_bCaptureAvailable ) 	g_CameraDriver.SetState( STREAM_CAPTURE, CSSTATE_PAUSE);
			if ( g_bStillAvailable ) 	g_CameraDriver.SetState( STREAM_STILL, CSSTATE_PAUSE);				

			WriteI2C ( 7, 0xffef, 'a' );
			initVM010();

			
            return 0;
        case WM_ACTIVATE:
            return 0;
        case WM_CHAR:
            if(wParam == 'q')
                DestroyWindow(hwnd);
            return 0;
        case WM_DESTROY:
			WriteI2C ( 7, 0xffef, 'a' );
            if(g_bCaptureAvailable) g_CameraDriver.SetState( STREAM_CAPTURE, CSSTATE_STOP);
            if(g_bPreviewAvailable) g_CameraDriver.SetState( STREAM_PREVIEW, CSSTATE_STOP);
            if(g_bStillAvailable)   g_CameraDriver.SetState( STREAM_STILL, CSSTATE_STOP);
            // since the inter-thread communication between the ccamerastreamtest thread
            // and this thread is asyncrnous, we can assume the stops are completed correctly.
			Sleep(1000);
            PostQuitMessage(0);
			Sleep(1000);
            return 0;
        case WM_SIZE:
            return 0;
        case WM_COMMAND:
            int nCommand = LOWORD(wParam);
            if(nCommand >= 0 && nCommand < countof(gButtonInfo))
                RETAILMSG(1, ( TEXT("CameraApp : %s pushed\n"), gButtonInfo[nCommand].tszButtonName));

            // these are all of the button commands enumerated above.
            switch(nCommand)
            {
				case COMMAND_COL_NAT:
						RETAILMSG(1,(TEXT("Set color for ... COMMAND_COL_NAT\r\n")));
						g_CameraDriver.SetRGBValues((float)1.3,(float)1,(float)0.8);
						break;
				case COMMAND_COL_NEO:
						RETAILMSG(1,(TEXT("Set color for ... COMMAND_COL_NEO\r\n")));
						g_CameraDriver.SetRGBValues((float)1,(float)1,(float)1.9);
						break;
				case COMMAND_COL_LAM:
						RETAILMSG(1,(TEXT("Set color for ... COMMAND_COL_LAM\r\n")));
						g_CameraDriver.SetRGBValues((float)0.6,(float)1,(float)2.1);			
						break;
				case COMMAND_COL_111:
						RETAILMSG(1,(TEXT("Set color for ... COMMAND_COL_111\r\n")));
						g_CameraDriver.SetRGBValues((float)1,(float)1,(float)1);			
						break;

                case COMMAND_CAPTURERUN:
					nover = 0;
					halbe = 0;
					if ( g_bCaptureAvailable )  g_CameraDriver.SetState( STREAM_CAPTURE, CSSTATE_PAUSE);
                    if ( g_bPreviewAvailable && FALSE == g_CameraDriver.SetState( STREAM_PREVIEW, CSSTATE_RUN))
                    {
                        RETAILMSG(1, (TEXT( "CameraApp : SetState run on the preview failed")));
                        PostQuitMessage(0);
                        return 0;
                    }
                    break;			
					
                case COMMAND_CAPTUREPAUSE:
					halbe = 0;
					if ( g_bPreviewAvailable )  g_CameraDriver.SetState( STREAM_PREVIEW, CSSTATE_PAUSE);
					if ( g_bCaptureAvailable )  g_CameraDriver.SetState( STREAM_CAPTURE, CSSTATE_PAUSE);

                    
					break;

				case 0x1113:
					{
						DWORD dw, dw1 = 0, t;
						USHORT hlp[10];

						

						dw = 0;
						for (dw = 0, t=0; t<2; t++) {
							if ( hlp[t] ) dw*=16; else break;

							if ((hlp[t]>='0') && (hlp[t]<='9'))
								dw += (hlp[t]-'0');
							if ((hlp[t]>='a') && (hlp[t]<='f'))
								dw += (0xa + hlp[t]-'a');
							if ((hlp[t]>='A') && (hlp[t]<='F'))
								dw += (0xa + hlp[t]-'A');
						}

						RETAILMSG(1,(TEXT("rd: %s\r\n"), hlp));

						g_CameraDriver.ReadRegister(dw, &dw);
						dw&=0xffff;
//changed               wsprintf(hlp, L"%x", dw);
						wsprintf(LPWSTR(hlp), L"%x", dw);
						SendMessage(wval, WM_SETTEXT, 0, (LPARAM) hlp); 
					}
					break;
				case 0x1114:
					{
						DWORD dw, dw1;
						USHORT hlp[10],t;
						
						SendMessage(wreg, WM_GETTEXT, 5, (LPARAM) hlp); 
						for (dw = 0, t=0; t<2; t++) {
							if ( hlp[t] ) dw*=16; else break;

							if ((hlp[t]>='0') && (hlp[t]<='9'))
								dw += (hlp[t]-'0');
							if ((hlp[t]>='a') && (hlp[t]<='f'))
								dw += (0xa + hlp[t]-'a');
							if ((hlp[t]>='A') && (hlp[t]<='F'))
								dw += (0xa + hlp[t]-'A');
						}

						SendMessage(wval, WM_GETTEXT, 5, (LPARAM) hlp); 

						for (dw1 = 0, t=0; t<4; t++) {
							if ( hlp[t] ) dw1*=16; else break;

							if ((hlp[t]>='0') && (hlp[t]<='9'))
								dw1 += (hlp[t]-'0'); 
							if ((hlp[t]>='a') && (hlp[t]<='f'))
								dw1 += (0xa + hlp[t]-'a'); 
							if ((hlp[t]>='A') && (hlp[t]<='F'))
								dw1 += (0xa + hlp[t]-'A'); 
						}
						RETAILMSG(1,(TEXT("wr: 0x%x -> 0x%x\r\n"), dw, dw1));

						g_CameraDriver.WriteRegister(dw, dw1);
					}
				break;
				
				case 0x1115:
					{
						 
						memset(&ofn, 0, sizeof(ofn));
						ofn.lStructSize       = sizeof(OPENFILENAME);
						ofn.hwndOwner         = NULL; // hDlg;
						ofn.lpstrFilter       = szFilter;
						ofn.lpstrCustomFilter = NULL;
						ofn.nFilterIndex      = 1;
						ofn.lpstrFile         = pszPath;
						ofn.nMaxFile          = MAX_PATH;
						ofn.lpstrInitialDir   = L"\\NandFlash\\";
						ofn.lpstrTitle        = L"Register File";
						ofn.Flags             = OFN_HIDEREADONLY | OFN_FILEMUSTEXIST;
						ofn.lpfnHook          = NULL; // OFNHookProc;
						ofn.lpstrDefExt       = TEXT("txt");
					    
						if ( GetOpenFileName( &ofn ) ) {
							ParseFile(pszPath);
							SaveRegistryData(pszPath, 1);
							if ( ! haswarn ) {
								MessageBox(NULL, L"Call \\windows\\savereg for persistend registry !", L"Attention", MB_OK);
								haswarn = TRUE;
							}
						}
					}
					break;

				case 0x1116:
#if 0									
					{
						SaveRegistryData(L"\0\0\0");
					}					
#else
					{
						BOOL checked = IsDlgButtonChecked(hwnd, 0x1116);
						if (checked) {
							CheckDlgButton(hwnd, 0x1116, BST_UNCHECKED);
							EnableWindow(rf, FALSE);
							SaveRegistryData(pszPath, 0);
						} else {
							CheckDlgButton(hwnd, 0x1116, BST_CHECKED);
							EnableWindow(rf, TRUE);
							SaveRegistryData(pszPath, 1);
						}
					}
					if ( ! haswarn ) {
						MessageBox(NULL, L"Call \\windows\\savereg for persistend registry !", L"Attention", MB_OK);
						haswarn = TRUE;
					}					
#endif					
					break;
					
				case 0x1117:
					{
						Singlechecked = IsDlgButtonChecked(hwnd, 0x1117);
						if (Singlechecked) {
							CheckDlgButton(hwnd, 0x1117, BST_UNCHECKED);
							WriteI2C ( 7, 0xffef, 'a' );
							Singlechecked = FALSE;

						} else {
							CheckDlgButton(hwnd, 0x1117, BST_CHECKED);
							WriteI2C ( 7, 0x10, 'o' );
							Singlechecked = TRUE;
						}
					}
					break;
				case 0x1234:
					if (runStream)
					{

					}
					break;
                case COMMAND_STILL_BMP:
					{
					halbe = 0;
					saveraw = FALSE;
					if ( g_bPreviewAvailable ) 	g_CameraDriver.SetState( STREAM_PREVIEW, CSSTATE_PAUSE);
					if ( g_bCaptureAvailable )  g_CameraDriver.SetState( STREAM_CAPTURE, CSSTATE_RUN);
					Sleep(400);
					
                    if ( g_bStillAvailable && FALSE == g_CameraDriver.TriggerCaptureEvent( STREAM_STILL))
                    {
                        RETAILMSG(1, (TEXT( "CameraApp : Running still failed")));
                        PostQuitMessage(0);
                        return 0;
                    }
					if ( !Singlechecked ) {
						Sleep(400);
						g_CameraDriver.SetState( STREAM_CAPTURE, CSSTATE_PAUSE);

					} else RETAILMSG(1,(TEXT("Wait forever to bmp\r\n")));						

                    break;
					}
                case COMMAND_STILL_RAW:	/* this functionality is misued for image processing */
					{
					wsprintf((LPWSTR)status, L"start image processing...");
					SendMessage(wreg10, WM_SETTEXT, 5, (LPARAM) status); 
				
					halbe = 0;
					saveraw = TRUE;
					if ( g_bPreviewAvailable ) 	g_CameraDriver.SetState( STREAM_PREVIEW, CSSTATE_PAUSE);
					if ( g_bCaptureAvailable )  g_CameraDriver.SetState( STREAM_CAPTURE, CSSTATE_RUN);
					
                    if ( g_bStillAvailable && FALSE == g_CameraDriver.TriggerCaptureEvent( STREAM_STILL))
                    {
                        RETAILMSG(1, (TEXT( "CameraApp : Running still failed")));
                        PostQuitMessage(0);
                        return 0;
                    }
					if ( !Singlechecked ) {
					
						g_CameraDriver.SetState( STREAM_CAPTURE, CSSTATE_PAUSE);

					} else RETAILMSG(1,(TEXT("Wait forever to RAW\r\n")));
                    break;
					}

					wregpopup = CreateWindow(L"Edit",  L"no processing done", WS_POPUP, 
                               10, 350, 150, 30,  // set size in WM_SIZE message 
                                hwnd, (HMENU) 0x1234, ghInstance, NULL); 
                default:
                    //RETAILMSG(1, (TEXT( "CameraApp : Unhandled button command.")));
                    break;   
            }
            // handle other buttons.
            return 0;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);

}


// ist die Auswahlbox fuer die Aufloesung --- hier wird nur eine feste verwendet !	
/****************************************************************************
    FontViewTest() - Window procedure for dialog box
******************************************************************************/
BOOL CALLBACK FormatSelectDialog(HWND hDlg, unsigned message, LONG wParam, LONG lParam)
{
    HWND    hID;
    int     iNum;
    BOOL    fRet = FALSE;
    int nPos;
    TCHAR **ptszCameraDrivers;
    int nDriverCount;
    TCHAR tszTempString[MAX_PATH];

    switch (message) 
    {
        case WM_INITDIALOG:           

            g_CameraDriver.DetermineCameraAvailability();

            g_CameraDriver.GetDriverList(&ptszCameraDrivers, &nDriverCount);

            // populate the preview combo box
            hID = GetDlgItem(hDlg, IDC_COMBO1);
            SendMessage(hID, CB_RESETCONTENT, 0, 0);

            for(iNum = 0; iNum < nDriverCount; iNum++)
            {
                nPos = SendMessage(hID, CB_INSERTSTRING, -1, (LPARAM)ptszCameraDrivers[iNum]);
            }
            SendMessage(hID, CB_SETCURSEL, 0, 0);
            g_nCameraDriverIndex = 0;

            hID = GetDlgItem(hDlg, IDC_COMBO2);
            SendMessage(hID, CB_RESETCONTENT, 0, 0);

            hID = GetDlgItem(hDlg, IDC_COMBO3);
            SendMessage(hID, CB_RESETCONTENT, 0, 0);

            hID = GetDlgItem(hDlg, IDC_COMBO4);
            SendMessage(hID, CB_RESETCONTENT, 0, 0);

            g_CameraDriver.Cleanup();

            fRet = TRUE;
            break;

        case WM_COMMAND:
            switch(LOWORD(wParam)) 
            {
                case IDC_COMBO1:
                {
                    g_CameraDriver.DetermineCameraAvailability();

                    hID = GetDlgItem(hDlg, IDC_COMBO1);
                    g_nCameraDriverIndex = SendMessage(hID, CB_GETCURSEL, 0, 0);
                    g_CameraDriver.SelectCameraDevice( g_nCameraDriverIndex );

                    if(FALSE == g_CameraDriver.InitializeDriver())
                    {
                        RETAILMSG(1, (TEXT( "CameraApp : Initializing the driver failed.")));
                        PostQuitMessage(0);
                        return 0;
                    }

                    g_bPreviewAvailable = g_CameraDriver.AvailablePinInstance(STREAM_PREVIEW);
                    g_bCaptureAvailable = g_CameraDriver.AvailablePinInstance(STREAM_CAPTURE);
                    g_bStillAvailable = g_CameraDriver.AvailablePinInstance(STREAM_STILL);

                    // reset the list box content for all of the mode selections
                    hID = GetDlgItem(hDlg, IDC_COMBO2);
                    SendMessage(hID, CB_RESETCONTENT, 0, 0);

                    hID = GetDlgItem(hDlg, IDC_COMBO3);
                    SendMessage(hID, CB_RESETCONTENT, 0, 0);

                    hID = GetDlgItem(hDlg, IDC_COMBO4);
                    SendMessage(hID, CB_RESETCONTENT, 0, 0);

                    // now that the driver is selected, we can populate the list boxes with the supported formats.

                    if(g_bPreviewAvailable)
                    {
                        // populate the preview combo box
                        hID = GetDlgItem(hDlg, IDC_COMBO2);
                        for(iNum = 0; iNum < g_CameraDriver.GetNumSupportedFormats(STREAM_PREVIEW); iNum++)
                        {
                            _itot(iNum, tszTempString, 10);
                            nPos = SendMessage(hID, CB_INSERTSTRING, -1, (LPARAM)tszTempString);
                        }
                        SendMessage(hID, CB_SETCURSEL, 0, 0);
                        g_nPreviewFormatIndex = SendMessage(hID, CB_GETCURSEL, 0, 0);
                    }

                    if(g_bCaptureAvailable)
                    {
                        // populate the capture combo box
                        hID = GetDlgItem(hDlg, IDC_COMBO3);
                        for(iNum = 0; iNum < g_CameraDriver.GetNumSupportedFormats(STREAM_CAPTURE); iNum++)
                        {
                            _itot(iNum, tszTempString, 10);
                            nPos = SendMessage(hID, CB_INSERTSTRING, -1, (LPARAM)tszTempString);
                        }
                        SendMessage(hID, CB_SETCURSEL, 0, 0);
                        g_nCaptureFormatIndex = SendMessage(hID, CB_GETCURSEL, 0, 0);
                    }

                    if(g_bStillAvailable)
                    {
                        // populate the still combo box
                        hID = GetDlgItem(hDlg, IDC_COMBO4);
                        for(iNum = 0; iNum < g_CameraDriver.GetNumSupportedFormats(STREAM_STILL); iNum++)
                        {
                            _itot(iNum, tszTempString, 10);
                            nPos = SendMessage(hID, CB_INSERTSTRING, -1, (LPARAM)tszTempString);
                        }
                        SendMessage(hID, CB_SETCURSEL, 0, 0);
                        g_nStillFormatIndex = SendMessage(hID, CB_GETCURSEL, 0, 0);
                    }
                    g_CameraDriver.Cleanup();

                    break;
                }

                case IDC_COMBO2:
                    hID = GetDlgItem(hDlg, IDC_COMBO2);
                    g_nPreviewFormatIndex = SendMessage(hID, CB_GETCURSEL, 0, 0);
                    break;
                case IDC_COMBO3:
                    hID = GetDlgItem(hDlg, IDC_COMBO3);
                    g_nCaptureFormatIndex = SendMessage(hID, CB_GETCURSEL, 0, 0);
                    break;
                case IDC_COMBO4:
                    hID = GetDlgItem(hDlg, IDC_COMBO4);
                    g_nStillFormatIndex = SendMessage(hID, CB_GETCURSEL, 0, 0);
                    break;

                // close the dialog.
                case IDC_BUTTON1:
                    EndDialog(hDlg, TRUE);
                    fRet = TRUE;
                    break;
            }
            break;
        // we've recieved a WM_CLOSE, so exit.
        case WM_CLOSE:
            EndDialog(hDlg, TRUE);
            fRet = TRUE;
            break;
        default:
            break;
    }

    return (fRet);
}


void InitCam ( void ) {
	TCHAR **ptszCameraDrivers;
	int nDriverCount;

	ReadRegistryData(&camType, &useReg);
	
	g_CameraDriver.DetermineCameraAvailability();
	g_CameraDriver.GetDriverList(&ptszCameraDrivers, &nDriverCount);
	g_nCameraDriverIndex = 0;

	g_CameraDriver.Cleanup();
	g_CameraDriver.DetermineCameraAvailability();
	if(FALSE == g_CameraDriver.InitializeDriver())
	{
		RETAILMSG(1, (TEXT( "CameraApp : Initializing the driver failed.")));
		PostQuitMessage(0);
		return;
	}
	g_CameraDriver.SetRGBValues(1,1,1);
	// so kann auch der kammeratyp bestimmt werden ...
//	g_CameraDriver.ReadRegister(0xf, &camType);

	g_bPreviewAvailable = g_CameraDriver.AvailablePinInstance(STREAM_PREVIEW);
	if(g_bPreviewAvailable)
	{
		g_nPreviewFormatIndex = 0;
	}
	g_bCaptureAvailable = g_CameraDriver.AvailablePinInstance(STREAM_CAPTURE);
	if(g_bCaptureAvailable)
	{
		g_nCaptureFormatIndex = 0;
	}
	g_bStillAvailable = g_CameraDriver.AvailablePinInstance(STREAM_STILL);
	if(g_bStillAvailable)
    {
		g_nStillFormatIndex = 0;
	}

	g_CameraDriver.Cleanup();
}



int WINAPI WinMain(
    HINSTANCE hInstance, 
    HINSTANCE hPrevInstance, 
    LPWSTR lpszCmdLine,
    int nCmdShow)
{
    TCHAR szClassName[] = TEXT("Camera");
	
    HWND hwnd;
    MSG msg;
    WNDCLASS wc;

	/* catch dll */
	//#define USEDLL
	#ifdef USEDLL
	HINSTANCE hLib=LoadLibrary((LPCWSTR)"opencv_demo_thumb.dll");
	char mod[MAXMODULE];

	GetModuleFileName((HMODULE)hLib, (LPTSTR)mod, MAXMODULE);
	RETAILMSG(1, (TEXT( "Get module: %s\n"), mod)); 

	if(hLib==NULL) 
	{
		RETAILMSG(1, (TEXT( "Could not load library!!!\n")));
	}
	else
	{
		RETAILMSG(1, (TEXT( "Load library \n")));
		RunOpenCV=(cfunc)GetProcAddress((HMODULE)hLib, (LPCWSTR)"RunOpenCV");
	}

	if(RunOpenCV==NULL) 
	{
		RETAILMSG(1, (TEXT( "Function could not be loaded \n")));
		FreeLibrary((HMODULE)hLib);
	}
	else
	{
		RunOpenCV();
	}

	FreeLibrary((HMODULE)hLib);
	#endif

	/* end of dll implementation */

    memset(&msg, 0, sizeof(MSG));
    ghInstance = hInstance;


		// kopieren der txt-datei nach \flashdisk
		{
			FILE *f;

			if ( ( f = fopen ("\\nandflash\\register-settings-mt9v024.txt", "r" ) ) == NULL) {
				RETAILMSG(1,(TEXT("copy to \\nandflash\\register-settings-mt9v024.txt\r\n")));
				CopyFile(L"\\windows\\register-settings-mt9v024.txt", L"\\nandflash\\register-settings-mt9v024.txt", TRUE);
				SetFileAttributes(L"\\nandflash\\register-settings-mt9v024.txt", FILE_ATTRIBUTE_NORMAL);
				MessageBox(NULL, L"Kopiere die Konfigurationsdatei register-settings-mt9v024.txt nach \\NandFlash", L"Hinweis", MB_OK);
			} else {
				fclose(f);
				RETAILMSG(1,(TEXT("\\nandflash\\register-settings-mt9v024.txt found !\r\n")));
			}
		}

// ist die Auswahlbox fuer die Aufloesung --- hier wird nur eine feste verwendet !		
//        DialogBox(hInstance, MAKEINTRESOURCE(IDD_FORMATSELECTPAGE), NULL, (DLGPROC)FormatSelectDialog);
		InitCam();
		
        memset(&wc, 0, sizeof(WNDCLASS));
        wc.lpfnWndProc = (WNDPROC) WndProc;
        wc.hInstance = ghInstance;
        wc.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
        wc.lpszClassName = szClassName;

        if (!RegisterClass(&wc))
            return 0;

		switch (camType) {
			case 0x01:	
#ifdef PHYCORE			
						hwnd = CreateWindow( szClassName, TEXT("Camera Test - VM-010-BW"), 
														WS_CAPTION | WS_CLIPCHILDREN | WS_VISIBLE | WS_SYSMENU,CW_USEDEFAULT, CW_USEDEFAULT,
														CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, ghInstance, NULL);
#else
									
						hwnd = CreateWindow( szClassName, TEXT("Camera Test - VM-010-BW-LVDS"), 
														WS_CAPTION | WS_CLIPCHILDREN | WS_VISIBLE | WS_SYSMENU,CW_USEDEFAULT, CW_USEDEFAULT,
														CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, ghInstance, NULL);									
#endif
						break;
			default:	
#ifdef PHYCORE			
						hwnd = CreateWindow( szClassName, TEXT("Camera Test - VM-010-COL"), 
								WS_CAPTION | WS_CLIPCHILDREN | WS_VISIBLE | WS_SYSMENU,CW_USEDEFAULT, CW_USEDEFAULT,
								CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, ghInstance, NULL);
#else								
						hwnd = CreateWindow( szClassName, TEXT("Camera Test - VM-010-COL-LVDS"), 
								WS_CAPTION | WS_CLIPCHILDREN | WS_VISIBLE | WS_SYSMENU,CW_USEDEFAULT, CW_USEDEFAULT,
								CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, ghInstance, NULL);								
#endif								
						break;
		}

        ShowWindow(hwnd, nCmdShow);
        UpdateWindow(hwnd);
        SetForegroundWindow(hwnd);

        while(GetMessage(&msg, NULL, 0, 0))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        UnregisterClass(szClassName, ghInstance);
	g_CameraDriver.Cleanup();
    return (msg.wParam);
}

void Log(LPWSTR szFormat, ...)
{
    va_list va;

    WCHAR wszInfo[1024];

    va_start(va, szFormat);
    StringCchVPrintf(wszInfo, _countof(wszInfo), szFormat, va);
    va_end(va);

    OutputDebugString(wszInfo);
}
